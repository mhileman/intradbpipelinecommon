from optparse import OptionParser

parser = OptionParser()

parser.add_option(
    "-H", "--hostname",
    action="store",
    type="string",
    dest="hostname"
)
parser.add_option(
    "-u", "--token-alias",
    action="store",
    type="string",
    dest="alias"
)
parser.add_option(
    "-p", "--token-secret",
    action="store",
    type="string",
    dest="secret"
)
parser.add_option(
    "-P", "--project",
    action="store",
    type="string",
    dest="project"
)
parser.add_option(
    "-s", "--subject",
    action="store",
    type="string",
    dest="subject"
)
parser.add_option(
    "-e", "--session",
    action="store",
    type="string",
    dest="session"
)
parser.add_option(
    "-c", "--scans",
    action="store",
    type="string",
    dest="scans",
    help='Optional comma separated list of scans. You can ommit it or ' +
         'set to "all" to run for every scan.'
)
parser.add_option(
    "-U", "--submit-to",
    action="store",
    type="string",
    dest="execute",
    default='local',
    help='One of local or sge. Default local'
)
parser.add_option(
    "-x", "--existing",
    default="skip",
    action="store",
    type="string",
    help='If existing resources, skip, overwrite, or fail. Default skip.'
)
parser.add_option(
    "-b", "--build-root",
    default='/data/intradb/build',
    action="store",
    type="string",
    dest="build"
)
parser.add_option(
    "-l", "--log-root",
    default='/data/intradb/logs/pipeline',
    action="store",
    type="string",
    dest="log"
)

# parser = OptionParser()
# parser.add_option("-H", "--hostname", action="store", type="string", dest="hostname")
# parser.add_option("-p", "--project", action="store", type="string", dest="project")
# parser.add_option("-s", "--subject", action="store", type="string", dest="subject")
# parser.add_option("-e", "--session", action="store", type="string", dest="session")
# parser.add_option("-c", "--scans", action="store", type="string", dest="scans",
#     help='Optional comma separated list of scans. If unused or "all", convert all scans.')
# parser.add_option("-u", "--submission-type", action="store", type="string", dest="submission",
#     default='local', help='One of local or sge. Default local')
# parser.add_option("-b", "--build-root", action="store", type="string", dest="build",
#     default='/data/intradb/build')
# parser.add_option("-l", "--log-root", action="store", type="string", dest="log",
#     default='/data/intradb/logs/pipeline')
# parser.add_option("-x", "--existing", action="store", type="string", default="skip",
#     help='For existing NIFTI resources, either skip, overwrite, or fail. Default skip.')
# (opts, args) = parser.parse_args()

# TODO check for all manditory params
